<?php
require_once( plugin_dir_path( __FILE__ )."UC_CutterTool_Class.php");
require_once( plugin_dir_path( __FILE__ )."functions.php");

function work_tasks_by_category( $atts ) {
    global $wpdb;

    if (isset($_POST["delete"])){
        if (!current_user_can('delete_work_tasks')){
            echo "Nimate pravic za brisanje delovnih nalogov";
            die;
        }
        $data= array (
          'status' => 'izbrisan'
        );
        $where= array (
            'id' => $_POST["delete"]
        );
        $wpdb->update($wpdb->prefix."uc_work_tasks",$data,$where);
    }

    $a = shortcode_atts( array(
        'category_id' => 'error'
    ), $atts );

    if (isset($_POST["opravljeni"]) && $_POST["opravljeni"]=='true'){
        $sql = "SELECT * FROM ".$wpdb->prefix . "uc_work_tasks WHERE category_id=".$a['category_id']." AND status <>'izbrisan';";
    }
    else {
        $sql = "SELECT * FROM ".$wpdb->prefix . "uc_work_tasks WHERE category_id=".$a['category_id']." AND status <>'izbrisan' AND status<> 'opravljen';";
    }
    $res = $wpdb->get_results($sql);


    /// SKUPNI ČAS //
    $skupaj=0;
    $vseSkupaj=0;
    foreach($res as $worktask){

        $sql = "SELECT * FROM ".$wpdb->prefix . "uc_toolusetime,".$wpdb->prefix."uc_tools,".$wpdb->prefix."uc_positions,".$wpdb->prefix."uc_work_tasks WHERE ".$wpdb->prefix."uc_positions.work_task_id= ".$worktask->id ." AND ".$wpdb->prefix."uc_tools.work_task_id = ". $worktask->id . " AND ".$wpdb->prefix."uc_toolusetime.tool_id=".$wpdb->prefix."uc_tools.id AND ".$wpdb->prefix."uc_work_tasks.id=".$worktask->id.";";
        $tools = $wpdb->get_results($sql);
        foreach ($tools as $tool){
                $skupaj += $tool->time * $tool -> kolicina;
        }

        $hours = intval($skupaj/60/60);
        $minutes = intval(($skupaj/60) - $hours*60);
        $seconds = intval($skupaj - $hours*60*60 - $minutes*60);
        $worktask->skupaj= "<strong>".$hours."h ".$minutes."m ".$seconds."s</strong>";
        $vseSkupaj+=$skupaj;
        $skupaj=0;
    }

    $hours = intval($vseSkupaj/60/60);
    $minutes = intval(($vseSkupaj/60) - $hours*60);
    $seconds = intval($vseSkupaj - $hours*60*60 - $minutes*60);
    $vseSkupaj = "<br/>Skupni čas vseh nalogov <strong>".$hours."h ".$minutes."m ".$seconds."s</strong>";
    /// SKUPNI ČAS END //


    $workTasksList="";
    $workTasksList.="<table>";
    $workTasksList.="<th>Nalog</th>";
    $workTasksList.="<th width='110'>Ustvarjen</th>";
    $workTasksList.="<th width='150'>Skupni čas</th>";
    $workTasksList.="<th width='110'>Status</th>";
    if (current_user_can('create_work_tasks'))
        $workTasksList.="<th width='65' class='hide_on_print'>Izbriši</th>";
    foreach($res as $workTask){
        $workTasksList.="<tr>";
        $workTasksList.="<td><a href='".home_url()."/delovni-nalog-pozicije/?id=".$workTask->id."'>".$workTask->name."</a></td>";
        $workTasksList.="<td>".$workTask->time_created."</td>";
        $workTasksList.="<td>".$workTask->skupaj."</td>";
        $workTasksList.="<td class='stat_".$workTask->status."'>".$workTask->status."</td>";
        if (current_user_can('create_work_tasks')) {
            $workTasksList .= "<td  class='hide_on_print'>
                            <form id='izbrisi-form' method='post'>
                                <a onclick='document.getElementById(\"izbrisi-form\").submit();'>Izbriši</a>
                                <input type='hidden' name='delete' value='" . $workTask->id . "'>
                                <input type='hidden' name='opravljeni' value='" . $_POST["opravljeni"] . "'>
                            </form>
                        </td>";
        }
        $workTasksList.="</tr>";
    }

    //SKUPNI ČAS
    $workTasksList.="</table>";
    $workTasksList.=$vseSkupaj;

    $check = '';

    if(isset($_POST['opravljeni']) && $_POST['opravljeni'] == 'true') {
        $check = 'checked';
    }
    $workTasksList.="<form method='post'>
                        Opravljeni delovni nalogi so skriti. <input type='checkbox' name='opravljeni' id='show_all' value='true' onchange='submit()' $check/> <label for='show_all'>Pokaži vse</label>
                    </form>";

    return $workTasksList;
}
add_shortcode( 'worktasksbycategory', 'work_tasks_by_category' );


function work_task_detailed( $atts ) {
    $id=$_GET["id"];
    $positionid=$_GET["positionid"];
    global $wpdb;

    $sql = "SELECT * FROM ".$wpdb->prefix . "uc_work_tasks WHERE id=".$id.";";
    $work_task = $wpdb->get_row($sql);
    $sql2 = "SELECT * FROM ".$wpdb->prefix . "uc_positions WHERE id=".$positionid.";";
    $position = $wpdb->get_row($sql2);
    $user = get_user_by("id", $work_task->user_id)->data->display_name;

    //Change status
    $status="";
    if (isset($_POST["status"])){
        if (!current_user_can('change_work_tasks_status')){
            echo "Nimate pravic za spreminjanje statusa delovnih nalogov";
            die;
        }
        if ($work_task->status == "opravljen")
            $status="neopravljen";
        if ($work_task->status == "neopravljen")
            $status="opravljen";
        $data = array (
            'status' => $status
        );
        $where = array (
          'id' => $id
        );
        $wpdb->update($wpdb->prefix."uc_work_tasks",$data, $where );
    }
    $work_task = $wpdb->get_row($sql);
    //END Change status
    //Button change text
    $updateStatusButtonText="";
    if ($work_task->status == "opravljen"){
        $updateStatusButtonText="neopravljen";
    }
    if ($work_task->status == "neopravljen"){
        $updateStatusButtonText="opravljen";
    }
    //END Button change text

    $sql = "SELECT * FROM ".$wpdb->prefix . "uc_tools WHERE work_task_id=".$id." AND position_id=".$positionid.";";
    $tools = $wpdb->get_results($sql);

    ?>
    <a class="hide_on_print" href="<?php echo home_url()?>/delovni-nalog-pozicije/?id=<?php echo $id?>" xmlns="http://www.w3.org/1999/html">Nazaj</a>
    <?php
    if(empty($tools))
        return "Ni orodja za ta nalog";
    ?>
    <h2 id="nalog-header">
        <span class="red-font">Nalog:</span> <?php echo $work_task->name ?> (<?php echo $work_task->time_created ?>) <br/>
        <span class="red-font">Pozicija:</span> <?php echo $position->name ?></h2>
    <h3><span class="red-font">Orodja:</span></h3>
    <table>
        <tr><th width="50">T</th><th>Ime</th><th width="150">Čas v uporabi</th></tr>
        <?php
        //Color coding:
        $maxColor = 63; //RED
        $maxTime = 5*60*60; //4ure


        $c = 1;
        $skupaj = 0;
        foreach($tools as $tool){
            $sql = "SELECT * FROM ".$wpdb->prefix . "uc_toolusetime WHERE tool_id=".$tool->id.";";
            $usetime = $wpdb->get_row($sql);


            $lightness = intval($tool->time/$maxTime*$maxColor);
            if($lightness > 90)
                $lightness = 90;
            $lightness .="%";

            $hours = intval($usetime->time/60/60);
            $minutes = intval(($usetime->time/60) - $hours*60);
            $seconds = intval($usetime->time - $hours*60*60 - $minutes*60);
            echo "<tr><td>$tool->num</td><td>$tool->name</td><td class='uporabaUr_" . date("H", $usetime->time) . "' style='color: hsl(0, 100%, $lightness)'>".$hours."h ".$minutes."m ".$seconds."s</td></tr>";
            $c++;
            $skupaj += $usetime->time;
        }

        $hours = intval($skupaj/60/60);
        $minutes = intval(($skupaj/60) - $hours*60);
        $seconds = intval($skupaj - $hours*60*60 - $minutes*60);
        echo "<tr><td></td><td style='text-align: right'><strong>Skupaj:</strong></td><td><strong>".$hours."h ".$minutes."m ".$seconds."s</strong></td></tr>";

        ?>

    </table>
    <?php

    return;
}

add_shortcode( 'worktaskdetailed', 'work_task_detailed' );



function work_task_positions( $atts ) {

    //quick fix za količino

    ?>
    <script>
        function izracun(posid) {

            var kolicina = document.getElementById("kolicina" + posid).value;

            var cas = document.getElementById("cas"  + posid).innerText;
            var hours = parseInt(cas.split("h")[0]);
            var minutes = parseInt(cas.split("h")[1].split("m")[0]);
            var seconds = parseInt(cas.split("m")[1].split("s")[0]);

            cas = (hours*60*60) + (minutes*60) + seconds;

            var time = cas * kolicina;

            document.getElementById("casskupaj" + posid).value = time;

            var h = parseInt(time/60/60);
            var m = parseInt(time/60 - h*60);
            var s = parseInt(time - h*60*60 - m*60);

            document.getElementById("skupaj" + posid).innerHTML =  h + "h " + m + "m " + s + "s";

        }
    </script>
    <?php
    global $wpdb;
    $id=$_GET["id"];

    /*position delete*/
    if (isset($_POST["delete"])&& !isset($_POST["update_statuses"])){
        if (!current_user_can('delete_work_tasks')){
            echo "Nimate pravic za brisanje delovnih nalogov";
            die;
        }
        $where= array (
            'id' => $_POST["delete"]
        );
        $wpdb->delete($wpdb->prefix."uc_positions",$where);
        $where= array (
            'position_id' => $_POST["delete"]
        );
        $wpdb->delete($wpdb->prefix."uc_tools",$where);
        $wpdb->last_result;
    }

    /* position delete end */
    /* Position statuses */
    if (isset($_POST['update_statuses'])){
        $table=$wpdb->prefix."uc_positions";
        $data= array(
            'status'=> 'neopravljen',
        );
        $where = array(
            'work_task_id' => $id
        );
        $wpdb->update($table,$data,$where);

        $data= array(
            'status'=> 'opravljen',
        );
        $positionStatDone = $_POST['position_statuses'];
        if($positionStatDone != null){
            foreach($positionStatDone as $position_id){
                $where = array(
                    'work_task_id' => $id,
                    'id' => $position_id
                );
                $wpdb->update($table,$data,$where);
            }
        }
        $count = $_POST['posid'];
        if($count != null && isset($_POST["kolicina"])){
            $i = 0;
            foreach($count as $position_id){
                $data= array(
                    'kolicina'=> $_POST["kolicina"][$i],
                );
                $where = array(
                    'id' => $position_id
                );
                $wpdb->update($table,$data,$where);
                $i++;
            }
        }

        $sql="SELECT * FROM ". $wpdb->prefix. "uc_positions WHERE status='neopravljen' AND work_task_id=".$id;
        $positions=$wpdb->get_results($sql);

        if (empty($positions)){
            $table=$wpdb->prefix."uc_work_tasks";
            $where = array(
                'id' => $id,
            );
            $data = array (
                'status' => 'opravljen'
            );
            $wpdb->update($table,$data,$where);
        }
        else{
            $table=$wpdb->prefix."uc_work_tasks";
            $where = array(
                'id' => $id,
            );
            $data = array (
                'status' => 'neopravljen'
            );
            $wpdb->update($table,$data,$where);
        }
    }
    /* END Position statuses */


    $sql = "SELECT * FROM ".$wpdb->prefix . "uc_work_tasks WHERE id=".$id.";";
    $work_task = $wpdb->get_row($sql);
    $sql2 = "SELECT * FROM ".$wpdb->prefix . "uc_positions WHERE work_task_id=".$id." ORDER BY name ASC;";
    $positions = $wpdb->get_results($sql2);

    $user = get_user_by("id", $work_task->user_id)->data->display_name;

    //Change status
    $status="";
    if (isset($_POST["status"])){
        if (!current_user_can('change_work_tasks_status')){
            echo "Nimate pravic za spreminjanje statusa delovnih nalogov";
            die;
        }
        if ($work_task->status == "opravljen")
            $status="neopravljen";
        if ($work_task->status == "neopravljen")
            $status="opravljen";
        $data = array (
            'status' => $status
        );
        $where = array (
            'id' => $id
        );
        $wpdb->update($wpdb->prefix."uc_work_tasks",$data, $where );
    }
    $work_task = $wpdb->get_row($sql);
    //END Change status
    //Button change text
    $statusText="neopravljen";
    if (!empty($work_task->status)){
        $statusText=$work_task->status;
    }
    //END Button change text


    if(empty($positions))
        return "Ni pozicij za ta nalog";

    $sql3= "SELECT * FROM ".$wpdb->prefix."uc_categories WHERE id=".$work_task->category_id.";";
    $category=$wpdb->get_row($sql3);
    ?>
    <a class="hide_on_print" href="<?php echo home_url()."/".$category->permalink_name?>/">Nazaj</a>
    <h6 class="red-font"><?php echo $category->name; ?></h6>
    <h2 id="nalog-header">
        <span class="red-font">Nalog:</span> <?php echo $work_task->name ?></h2>
    <h5>
        <span class="red-font">Ustvaril:</span> <?php echo $user ?> <span class="small">(<?php echo $work_task->time_created ?>)</span>
        <br/>
        <span class="red-font">Status:</span> <span class="stat_<?php echo $work_task->status ?>" ><?php echo $work_task->status ?></span>
        <?php
        if (current_user_can('create_work_tasks')) {
            ?>
            <form method="post" class="form-right-align">
                <button type="button" class="yellowbg" onclick="window.scrollTo(0,document.body.scrollHeight)">Dodaj
                    xml-je
                </button>
                <input type="hidden" name="status" value="<?php echo $id ?>">
            </form>
            <?php
        }
        ?>
    </h5>
    <form class="all-positions" method="post">
    <table>
        <th width="auto">Pozicija</th>
        <th width="65">Kol.</th>
        <th width="145">Čas pozicije</th>
        <th width="145">Čas skupaj</th>
        <th  width="140" id="oznaci-column">
            <input type="checkbox" id="checkbox-status" onclick="chooseall('position_status')" /> <label for="checkbox-status" >Status</label>
        </th>
        <?php
        if (current_user_can('create_work_tasks'))
        {
            ?>
            <th width = "65" class='hide_on_print'> Izbriši</th>
            <?php
        }

        /// SKUPNI ČAS //
        $vseskupaj=0;
        $skupaj=0;
        $kolicina_skupaj = 0;
        foreach($positions as $position){
                $sql = "SELECT * FROM ".$wpdb->prefix . "uc_toolusetime,".$wpdb->prefix."uc_tools WHERE ".$wpdb->prefix."uc_toolusetime.tool_id=".$wpdb->prefix."uc_tools.id AND  position_id=".$position->id.";";
                $tools = $wpdb->get_results($sql);

                foreach ($tools as $tool){
                    $skupaj += $tool->time;
                }

                $hours = intval($skupaj/60/60);
                $minutes = intval(($skupaj/60) - $hours*60);
                $seconds = intval($skupaj - $hours*60*60 - $minutes*60);

                $position->skupaj= "<strong>".$hours."h ".$minutes."m ".$seconds."s</strong>";

                $kolicina_skupaj = intval($skupaj) * $position->kolicina;

                $hours1 = intval($kolicina_skupaj / 60 / 60);
                $minutes1 = intval(($kolicina_skupaj / 60) - $hours1 * 60);
                $seconds1 = intval($kolicina_skupaj - $hours1 * 60 * 60 - $minutes1 * 60);

                $vseskupajskolicino = "<strong>" . $hours1 . "h " . $minutes1 . "m " . $seconds1 . "s</strong>";

               ;

                $vseskupaj += $kolicina_skupaj;
                $kolicina_skupaj = 0;
                $skupaj=0;

            //SKUPNI ČAS END //
            $check="";
            $stat = "neopravljen"; // v primeru, da v bazi manjka ta podatek
            if ($position->status=="opravljen"){
                $check="checked";
                $stat=$position->status;
            }

            ?>

            <tr>
                <td>
                    <a class="<?php echo $position->status ?>"
                       href='<?php echo home_url() ?>/delovni-nalog?id=<?php echo $work_task->id ?>&positionid=<?php echo $position->id ?>'>Poz. <?php echo $position->name ?></a>
                </td>
                <td>
                    <input id="kolicina<?php echo $position->id ?>" name="kolicina[]" placeholder="1"
                           value="<?php echo $position->kolicina ?>"
                           onchange="izracun(<?php echo $position->id ?>)"/>
                </td>
                <td id="cas<?php echo $position->id ?>">
                    <?php echo $position->skupaj ?>
                </td>
                <td id="skupaj<?php echo $position->id ?>" style="font-weight: bold">
                    <?php echo $vseskupajskolicino ?>
                </td>
                <td>
                    <input class="position_status" name="position_statuses[]" <?php echo $check ?>
                           id="checkbox_<?php echo $position->id ?>"
                           value="<?php echo $position->id ?>_<?php echo $position->status ?>" type='checkbox'/>
                    <label class="stat_<?php echo $stat ?>"
                           for="checkbox_<?php echo $position->id ?>"> <?php echo $stat ?> </label>
                </td>
                <?php
                if (current_user_can('create_work_tasks')) {
                    ?>
                    <td class='hide_on_print'>
                        <a onclick='izbrisi(<?php echo $position->id ?>);'>Izbriši</a>
                    </td>
                    <?php
                }
                ?>
                <input id="casskupaj<?php echo $position->id ?>" name="casskupaj[]" value="" type="hidden"/>
                <input name="posid[]" value="<? echo $position->id ?>" type="hidden"/>
            </tr>
            <?php
        }
        $hours = intval($vseskupaj/60/60);
        $minutes = intval(($vseskupaj/60) - $hours*60);
        $seconds = intval($vseskupaj - $hours*60*60 - $minutes*60);
        $vseskupajkoncno= "<strong>".$hours."h ".$minutes."m ".$seconds."s</strong>";
        ?>
        <tr><td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <button type="submit" name="update_statuses" value="y">Shrani</button>
            </td>
            <?php
            if (current_user_can('create_work_tasks'))
            {
                ?>
                <td class='hide_on_print'></td>
                <?php
            }
            ?>
        </tr>


    </table>
        Skupni čas vseh pozicij: <?php echo $vseskupajkoncno ?>
    </form>
    <form id='izbrisi-form' method='post'>
        <input type='hidden' id="delete" name='delete' value=''>
    </form>

    <?php
}

add_shortcode( 'worktaskpositions', 'work_task_positions' );

function upload_xml_to_worktask_form( $atts ){
    if (!current_user_can('create_work_tasks'))
        return "<!-- Uporabnik nima pravic za upload XML datotek. -->";

    ?><div id="gorjak_form_field">
    <?php
    $uploadWidget = new UC_GorjakXMLWidget();
    $uploadWidget->showUploadForm(true);

    ?>
</div>
    <?php
}
add_shortcode( 'uploadxmltoworktaskform', 'upload_xml_to_worktask_form' );

function upload_xml_form( $atts ){
    the_widget("UC_GorjakXMLWidget");
}
add_shortcode( 'uploadxmlform', 'upload_xml_form' );

function upload_xml_to_worktask_action( $atts ){
    $id=$_GET["id"];

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES['xml_files'])){
        //do upload
        if (!current_user_can('create_work_tasks')){
            echo "Nimate pravic za nalaganje datotek in kreiranje delovnih nalogov";
            die;
        }
        $uploadWidget = new UC_GorjakXMLWidget();
        echo"<p>";
        $uploadWidget->uploadFilesAndParseXML($id);
        echo"</p>";
    }
    ?>
    <?php
}
add_shortcode( 'uploadxmltoworktaskaction', 'upload_xml_to_worktask_action' );
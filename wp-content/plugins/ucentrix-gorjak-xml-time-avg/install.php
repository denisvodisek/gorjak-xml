<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

global $uc_gorjak_xml_db_version;
$uc_gorjak_xml_db_version = '1.5';

function uc_gorjak_XML_install()
{
    global $uc_gorjak_xml_db_version;
    $installed_ver = get_option("uc_gorjak_xml_db_version");

    if ($installed_ver != $uc_gorjak_xml_db_version) {
        global $wpdb;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $charset_collate = $wpdb->get_charset_collate();

        $sqlInstall = "CREATE TABLE " . $wpdb->prefix . "uc_tools (
        id int(11) NOT NULL AUTO_INCREMENT,
        user_id int(11) NOT NULL ,
        work_task_id int(11) NOT NULL ,
        position_id int(11) NOT NULL ,
        num int(11) NOT NULL,
        name VARCHAR(200) NOT NULL,
        PRIMARY KEY id (`id`)
        ) $charset_collate;";

        dbDelta($sqlInstall);
        $sqlInstall = "CREATE TABLE " . $wpdb->prefix . "uc_toolusetime (
        id int(11) NOT NULL AUTO_INCREMENT ,
        tool_id int(11) NOT NULL ,
        time FLOAT NOT NULL,
        user_id int(11) NOT NULL ,
        created int(11) NOT NULL ,
        PRIMARY KEY (`id`)
        ) $charset_collate;";
        dbDelta($sqlInstall);

        $sqlInstall = "CREATE TABLE " . $wpdb->prefix . "uc_categories (
        id int(11) NOT NULL AUTO_INCREMENT ,
        name VARCHAR(200) NOT NULL,
        permalink_name VARCHAR(200) NOT NULL,
        PRIMARY KEY (`id`)
        ) $charset_collate;";
        dbDelta($sqlInstall);

        //Vstavimo kategorije
        insertCategories();

        $sqlInstall = "CREATE TABLE " . $wpdb->prefix . "uc_work_tasks (
        id int(11) NOT NULL AUTO_INCREMENT ,
        name varchar(200) NOT NULL ,
        status varchar(100) NOT NULL ,
        category_id int(11) NOT NULL ,
        user_id int(11) NOT NULL ,
        time_created varchar(200) NOT NULL ,
        PRIMARY KEY (`id`)
        ) $charset_collate;";
        dbDelta($sqlInstall);

        $sqlInstall = "CREATE TABLE " . $wpdb->prefix . "uc_positions (
        id int(11) NOT NULL AUTO_INCREMENT ,
        name varchar(200) NOT NULL ,
        work_task_id int(11) NOT NULL ,
        status varchar(100) NOT NULL ,
        kolicina int(11) NOT NULL DEFAULT '1',
        PRIMARY KEY (`id`)
        ) $charset_collate;";
        dbDelta($sqlInstall);


        add_option('uc_gorjak_xml_db_version', $uc_gorjak_xml_db_version);
    }
}

function insertCategories(){
    global $wpdb;

    $table=  $wpdb->prefix . "uc_categories";
    $data= array (
        'id'=>1,
        'name'=>"Tlačna orodja",
        'permalink_name'=>"tlacna-orodja"
    );
    $wpdb->insert( $table, $data);

    $data= array (
        'id'=>2,
        'name'=>"Obrezilna orodja",
        'permalink_name'=>"obrezilna-orodja"
    );
    $wpdb->insert( $table, $data);

    $data= array (
        'id'=>3,
        'name'=>"Elektrode",
        'permalink_name'=>"elektrode"
    );

    $wpdb->insert( $table, $data);

    $data= array (
        'id'=>4,
        'name'=>"Popravila",
        'permalink_name'=>"popravila"
    );
    $wpdb->insert( $table, $data);

}
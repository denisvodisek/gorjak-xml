<?php
/**
 * Created by PhpStorm.
 * User: denisvodisek
 * Date: 22.7.2016
 * Time: 10:57
 */
class UC_CutterTool{
    public $number;
    public $name;
    private $time;
    public $position;
    function __construct($na, $num, $t, $pos)
    {
        $this->name = $na;
        $this->number = $num;
        $this->time = floatval($t);
        $this->position = $pos;
    }

    public function addTime($t){
        $this->time += floatval($t);
    }
    public function getTime(){
        return $this->time;
    }
}



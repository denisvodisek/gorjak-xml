<?php
/*
Plugin Name: Gorjak XML Time Average
Plugin URI: http://u-centrix.com/
Description: Povpreči čase uporabe odorij v naloženih XMLjih
Version: 1.0
Author: U-centrix
Author URI: http://u-centrix.com/
License: GPLv2 or later
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require_once(plugin_dir_path(__FILE__)."install.php");
require_once( plugin_dir_path( __FILE__ )."shortcodes.php");
require_once( plugin_dir_path( __FILE__ )."UC_CutterTool_Class.php");


register_activation_hook(__FILE__, "uc_gorjak_XML_install");

function uc_gorjak_XML_update_db_check() {
  global $uc_gorjak_xml_db_version;
  if ( get_site_option( 'uc_gorjak_xml_db_version' ) != $uc_gorjak_xml_db_version ) {
    uc_gorjak_XML_install();
  }
}
add_action( 'plugins_loaded', 'uc_gorjak_XML_update_db_check' );


// Register style sheet.
add_action( 'wp_enqueue_scripts', 'uc_gorjak_XML_register_plugin_styles' );

function myplugin_update_db_check() {
  global $jal_db_version;
  if ( get_site_option( 'jal_db_version' ) != $jal_db_version ) {
    jal_install();
  }
}
add_action( 'plugins_loaded', 'myplugin_update_db_check' );


add_action('wp_head', 'hide_XML_if_operator');
function hide_XML_if_operator(){
    if(!current_user_can('create_work_tasks')){
        ?>
        <style>
            #menu-item-9{
                display:none;
            }
        </style>
        <?php
    }
}

/**
 * Register style sheet.
 */
function uc_gorjak_XML_register_plugin_styles() {
  wp_register_style( 'uc_gorjak_XML', plugins_url( 'ucentrix-gorjak-xml-time-avg/style.css' ) );
  wp_enqueue_style( 'uc_gorjak_XML' );

  wp_deregister_script('jquery');
  wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", false, null);
  wp_enqueue_script('jquery');

  wp_register_script('uc_gorjak_XML_s', plugins_url('ucentrix-gorjak-xml-time-avg/assets/scripts.js'));
  wp_enqueue_script('uc_gorjak_XML_s');
}

class UC_GorjakXMLWidget extends WP_Widget {
  private $tools = array();


  // constructor
  function __construct() {
    $widget_ops = array(
        'class_name' => '',
        'description' => 'Nalaganje XMLjev.',
    );
    parent::__construct('UC_GorjakXMLWidget', $name = "Gorjak XML Upload", $widget_ops);
  }

  // widget form creation
  function form($instance) {

  }

  // widget update
  function update($new_instance, $old_instance) {

    return $new_instance;
  }

  // widget display
  function widget($args, $instance) {
    $current_user = wp_get_current_user();

    ?>
      <p class="userGreeting">Pozdravljeni <? echo $current_user->display_name; ?>.</p>
    <div id="gorjak_form_field">
    <?php
      $this->showUploadForm();

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES['xml_files'])){
        //do upload
      if (!current_user_can('create_work_tasks')){
        echo "Nimate pravic za nalaganje datotek in kreiranje delovnih nalogov";
        die;
      }
        $this->uploadFilesToNewTask();
      }
    ?>
    </div>
    <?php
    //$this->showCurrentlyUploaded();
    //$this->showAllToolsInDB();
  }
  function clearDB(){
    global $wpdb;
    $wpdb->query("DELETE FROM `".$wpdb->prefix."uc_tools` WHERE user_id = ".get_current_user_id());
    $wpdb->query("DELETE FROM `".$wpdb->prefix."uc_toolusetime` WHERE user_id = ".get_current_user_id());
  }

  //Prejšnja funkcija ki prepiše bazo
  function saveToolsToDBrewrite(){
    global $wpdb;
      foreach($this->tools as $tool){
        //check if tool already in DB:
        $id=null;
        $sql = $wpdb->prepare("SELECT * FROM `".$wpdb->prefix."uc_tools` WHERE `name` LIKE %s AND user_id=".get_current_user_id().";", $tool->name);
        $res = $wpdb->get_col($sql);
        if($res){
          $id = $res[0];
        }else{
          //insert new tool
          $data = array('name' => $tool->name, 'num' => $tool->number, 'user_id' => get_current_user_id());
          $wpdb->insert($wpdb->prefix."uc_tools", $data);
          $id = $wpdb->insert_id;
        }

        //save time to DB:
        $timedata = array("tool_id" => intval($id),
            "user_id" => get_current_user_id(),
            "time" => $tool->getTime(),
            "created" => time());
        $dataformat = array("%d",
            "%d",
            "%f",
            "%d");
        $wpdb->insert($wpdb->prefix."uc_toolusetime", $timedata, $dataformat);

      }
  }

  function showUploadForm($inTask = false){
    $categories=$this->getCategoriesOptionsForSelect();
    if (!current_user_can('create_work_tasks')) {
        echo "Nimate pravic za nalaganje novih XMLjev.";
        return;
    }

    ?>
    <div>
      <?php if($inTask){ ?>
        <p>Dodaj xml-je za ta delovni nalog.</p>
      <?php }else{ ?>
        <p>Tukaj naložite XML datoteke. Program bo poiskal časovne zapise in jih združil v eno tabelo.</p>
      <?php } ?>

      <form method="post" enctype="multipart/form-data">

    <?php if(!$inTask){ ?>
        Kategorija <select name="category">
          <?php echo $categories ?>
        </select>
        <br/>
        Ime naloga <input id="input-form" type="text" name="work_task_name"/>
        <br/>
    <?php } ?>
        Naloži XML-je: <input type="file" name="xml_files[]" multiple>
        <br/>
        <input type="submit" value="Naloži">

      </form>
    </div>
    <?php
  }


  //če $work_task_id == null, se ustvari nov worktask. Če ne se posodobi star.
  function uploadFilesAndParseXML($work_task_id=null, $category_id=null, $work_task_name=null){
    $target_dir = dirname( __FILE__ )."/uploads/";
    $files = $_FILES["xml_files"];
    for($i = 0; $i < count($files['name']); $i++) {
      if(empty($files['name'][0]))
        break;
      $target_file = $target_dir . basename($files["name"][$i]);
      if($files["type"][$i] != "text/xml"){
        echo "Datoteka <strong>".basename($files["name"][$i])."</strong> ni tipa XML. Grem na naslednjo.<br/>";
        continue;
      }
      if (file_exists($target_file)) {
        unlink($target_file);
      }
      if (move_uploaded_file($files["tmp_name"][$i], $target_file)) {
        echo "Datoteka <strong>". basename( $_FILES["xml_files"]["name"][$i]). "</strong> je bila naložena.<br/>";
        $filename = basename( $_FILES["xml_files"]["name"][$i]);
        //parse XML
        $this->parseXml($target_file);
      } else {
        echo "Prišlo je do napake pri nalaganju datoteke.";
      }
    }
    if(empty($work_task_id)) {
      //nov task
        $work_task_id = $this->saveWorkTaskToDB($category_id, $work_task_name, $filename);
      $this->saveToolsToDB($work_task_id);
    }else{
      //worktask (delovni nalog) že obstaja
      //poglej če v worktasku obstajajo pozicije z istim imenom: Potem združi orodja
        mysql_query('START TRANSACTION');

        $allOk = true;
        $allOk = $allOk && $this->readToolsFromDBAndMerge($work_task_id);
        $allOk = $allOk && $this->saveToolsToDB($work_task_id);
        if($allOk)
            mysql_query('COMMIT'); // commits all queries
        else
            mysql_query('ROLLBACK'); // rollbacks everything

    }
    echo "<a href='".home_url()."/delovni-nalog-pozicije/?id=".$work_task_id."'>Na delovni nalog</a>";
    //
  }

  function uploadFilesToNewTask()
  {
    $category_id=$_POST["category"];
    $work_task_name=$_POST["work_task_name"];
    $this->uploadFilesAndParseXML(null, $category_id, $work_task_name);
  }

  function parseXml($path){
    $xml=simplexml_load_file($path) or die("Error: Cannot create object");
    $result = $xml->xpath("Toolpath");

    for($i=0; $i<count($result); $i++){
      $obj = $result[$i];

      $estTime = $obj->EstimatedMachiningTime->__toString();
      $toolNumber = $result[$i]->Cutter->ToolNumber->__toString();
      $name = $this->sanitizeToolName($result[$i]->Cutter->ToolName->__toString());
      $position = $xml->ProjectComment->__toString();
      if ($position=="")
        $position="Datoteka nima vpisane pozicije v projektnem komentarju";

      if(array_key_exists($position, $this->tools) && array_key_exists($name, $this->tools[$position])){
        $ct = $this->tools[$position][$name];
        $ct->addTime($estTime);
      }else{
        $ct = new UC_CutterTool($name, $toolNumber, $estTime, $position);
        $this->tools[$position][$name] = $ct;
      }
    }
  }

//Nova funkcija ki shrane orodja pod worktask
  function saveToolsToDB($work_task_id)
  {
    global $wpdb;
    $allOk = true;
    foreach ($this->tools as $positionName=>$positionTools) {
      //insert new position
      $positionData = array(
          'name' => $positionName,
          'work_task_id' => $work_task_id,
          'status' => 'neopravljen'
      );
      $allOk = $allOk && $wpdb->insert($wpdb->prefix . "uc_positions", $positionData);
      $idPosition = $wpdb->insert_id;

      foreach ($positionTools as $tool){
        //insert new tool
        $data = array('name' => $this->sanitizeToolName($tool->name), 'num' => $tool->number, 'user_id' => get_current_user_id(), 'work_task_id' => $work_task_id, 'position_id' => $idPosition);
        $wpdb->insert($wpdb->prefix . "uc_tools", $data);
        $id = $wpdb->insert_id;

        //save time to DB:
        $timedata = array("tool_id" => intval($id),
            "user_id" => get_current_user_id(),
            "time" => $tool->getTime(),
            "created" => time());
        $dataformat = array("%d",
            "%d",
            "%f",
            "%d");
        $allOk = $allOk && $wpdb->insert($wpdb->prefix . "uc_toolusetime", $timedata, $dataformat);
      }

    }
    return $allOk;
  }
  function sanitizeToolName($toolName){
      $newName =str_replace("Ø", "FI", $toolName);
      return $newName;
  }
  function readToolsFromDBAndMerge($work_task_id)
  {
    global $wpdb;
    $allOk = true;
      try {
          $positionsInWT = $wpdb->get_results(
              "
            SELECT * 
            FROM " . $wpdb->prefix . "uc_positions
            WHERE work_task_id = $work_task_id 
            "
          );

          foreach ($positionsInWT as $position) {
              $positionName = $position->name;
              //get all tools in WT, add them to the
              $tools = $wpdb->get_results(
                  "
                    SELECT * 
                    FROM " . $wpdb->prefix . "uc_tools
                    WHERE work_task_id = $work_task_id 
                        AND position_id = " . $position->id
              );

              foreach ($tools as $oneTool) {
                  $toolusetime = $wpdb->get_row(
                      "
                        SELECT * 
                        FROM " . $wpdb->prefix . "uc_toolusetime
                        WHERE tool_id = " . $oneTool->id
                  );
                  $toolName = $this->sanitizeToolName($oneTool->name);

                  if (array_key_exists($positionName, $this->tools) && array_key_exists($toolName, $this->tools[$positionName])) {
                      $ct = $this->tools[$positionName][$toolName];
                      $ct->addTime($toolusetime->time);
                  } else {
                      $ct = new UC_CutterTool($toolName, $oneTool->num, $toolusetime->time, $position->id);
                      $this->tools[$positionName][$toolName] = $ct;
                  }

                  //delete tool from DB
                  $delTool = array('id' => $oneTool->id);
                  $allOk = $allOk && $wpdb->delete($wpdb->prefix . "uc_tools", $delTool);

                  //delete tooltime from DB
                  $delToolTime = array('id' => $toolusetime->id);
                  $allOk = $allOk && $wpdb->delete($wpdb->prefix . "uc_toolusetime", $delToolTime);
              }

              //delete position from DB
              $delPosition = array('id' => $position->id);
              $allOk = $allOk && $wpdb->delete($wpdb->prefix . "uc_positions", $delPosition);
          }
      }catch(Exception $e){
          echo "Napaka: ", $e->getMessage();
          return false;
      }
    return $allOk;
  }

  function saveWorkTaskToDB($category_id,$work_task_name, $filename)
  {
    global $wpdb;
    $data = array('category_id' => $category_id, 'name' => $work_task_name, 'time_created' => date("d.m.Y, G:i:s"), 'user_id'=>get_current_user_id(), 'status' => 'neopravljen', 'fileName' => $filename);
    //preveri če vnos že obstaja
    $sqlcheck = $wpdb->get_row(
            "SELECT * 
                    FROM " . $wpdb->prefix . "uc_work_tasks
                    WHERE name = '" . $work_task_name ."' AND status != 'izbrisan'");
    if(sizeof($sqlcheck) > 0) {
        $id = $sqlcheck -> id;
        $wpdb->delete($wpdb->prefix . "uc_work_tasks", array('id' => $id));
        $wpdb->insert($wpdb->prefix . "uc_work_tasks", $data);
        $id = $wpdb->insert_id;
        return $id;
    }
    else {
        $wpdb->insert($wpdb->prefix . "uc_work_tasks", $data);
        $id = $wpdb->insert_id;
        return $id;
    }



  }

  function showAllToolsInDB()
  {
    global $wpdb;
    $sql = "SELECT tool.id, tool.name, tool.num, SUM(tut.time) as totaltime FROM `" . $wpdb->prefix . "uc_tools` as tool, `" . $wpdb->prefix . "uc_toolusetime` as tut
            WHERE tool.id = tut.tool_id AND tool.user_id=".get_current_user_id()." GROUP BY tut.tool_id ORDER BY totaltime DESC;";
    //$sql = "SELECT tool.id, tool.name, tool.num, SUM(tut.time) as totaltime FROM `" . $wpdb->prefix . "uc_tools` as tool, `" . $wpdb->prefix . "uc_toolusetime` as tut WHERE tool.id = tut.tool_id GROUP BY tut.tool_id ORDER BY tool;";
    $res = $wpdb->get_results($sql);

    if(count($res) == 0)
      return;
    ?>
    <h2>Seznam orodij za delovni nalog:</h2>
    <table id="gorjak_tools2">
      <tr><th width="50">T</th><th>Ime</th><th width="150">Čas v uporabi</th></tr>
      <?php
      //Color coding:
      $maxColor = 63; //RED
      $maxTime = 5*60*60; //4ure


      $c = 1;
      $skupaj=0;
      foreach ($res as $tool) {

        $lightness = intval($tool->totaltime/$maxTime*$maxColor);
        if($lightness > 90)
          $lightness = 90;
        $lightness .="%";

        $hours = intval($tool->totaltime/60/60);
        $minutes = intval(($tool->totaltime/60) - $hours*60);
        $seconds = intval($tool->totaltime - $hours*60*60 - $minutes*60);
        echo "<tr><td>$tool->num</td><td>$tool->name</td><td class='uporabaUr_" . date("H", $tool->totaltime) . "' style='color: hsl(0, 100%, $lightness)'>".$hours."h ".$minutes."m ".$seconds."s</td></tr>";
        $c++;
        $skupaj+= $tool->totaltime;
      }
      $hours = intval($skupaj/60/60);
      $minutes = intval(($skupaj/60) - $hours*60);
      $seconds = intval($skupaj - $hours*60*60 - $minutes*60);
      echo "<tr><td></td><td style='text-align: right'><strong>Skupaj:</strong></td><td><strong>".$hours."h ".$minutes."m ".$seconds."s</strong></td></tr>";
      ?>
    </table>
    <?php

    $current_user = wp_get_current_user();
    ?>
    <p class="userMade">Pripravil: <? echo $current_user->display_name; ?></p>
    <?php
  }

  function getCategoriesOptionsForSelect(){
    $returnOptions="";

    global $wpdb;
    $sql = "SELECT * FROM ".$wpdb->prefix . "uc_categories;";
    $res = $wpdb->get_results($sql);
    foreach ($res as $category){
      $returnOptions.="<option value='".$category->id."'>".$category->name."</option>";
    }

    return $returnOptions;
  }
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("UC_GorjakXMLWidget");'));
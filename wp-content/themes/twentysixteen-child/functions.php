<?php
function theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );


function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
            padding-bottom: 30px;
            width:299px;
            height:63px;
            background-size: auto;
        }
        body{
            background: white;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


global $wpdb;
$wpdb->show_errors(); //U-centrix: for debuging
?>
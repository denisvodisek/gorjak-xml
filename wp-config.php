<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gorjak-xml');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define("FS_METHOD", "direct");
define('AUTH_KEY',         'wmDx)o!9E^8+P1#L-DhvderO#:y_l{?&E9d{9Tt_kYDf,0w$}qU_:iCI)B_MRD+4');
define('SECURE_AUTH_KEY',  'FV8IwvgUd8ccW3Bd#bc*as@y;b+bq)++:{/-TQrPRZ*J|: 7eNZl|{KCIg[u+0~,');
define('LOGGED_IN_KEY',    'dIMiLTGrs#HL0|I1fv,FG#j0N;ssusYXlQnNm*hwA$H )w @kwn)IPl#+k{j+a!{');
define('NONCE_KEY',        '0|oX/Y<`-_gLqW%{#eGr}6wG)J.Fu{Bc0,zLs.dr^+l}C<VcC-_r.,>E71W<E 5R');
define('AUTH_SALT',        'a8?aM{Gaak^/={@L_`v+o2b8i#N]6|rT9*Y4pL?*/t8vZ*(l>Y+*[ XhDnO|^J0y');
define('SECURE_AUTH_SALT', '5$f:;]bY4c}Y92,;{wisPF,-:{{$+D}[<4%V#Z8|xoeE&+Lj:BwbI^zi)I!EA%gi');
define('LOGGED_IN_SALT',   '=U R>N=oZRrJf.Mv]bK2,M_+KwFu,u0j{!Go?Kc$<FlF<>+ZL4W}0?+<iC#|ZOup');
define('NONCE_SALT',       'V&Fsu-a-gV/gK/^GO5+;=+O3..V*|xy|(Lx@uaq=VTQ^H@BzaS1;;|rK ESK$nZc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
